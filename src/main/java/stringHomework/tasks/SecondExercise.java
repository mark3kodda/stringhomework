package main.java.stringHomework.tasks;

public class SecondExercise {

    public String intToString(int i){
        return String.valueOf(i);
    }

    public String doubleToString(double i){
        return String.valueOf(i);
    }

    public int sendingToInt(String str){
        return Integer.parseInt(str);
    }

    public double sendingToDouble(String str){
        return Double.parseDouble(str);
    }
}
