package main.java.stringHomework.tasks;

import java.util.Arrays;

public class ThirdExercise {

    public String[] replaceThreeWordsWith$ (String [] givenArr, int wordLength){
        int counter = 0;
        int [] indexesOfTargetWords = new int[givenArr.length];
        for (int i = 0; i < givenArr.length ; i++) {
            if(givenArr[i].length()==wordLength){
                indexesOfTargetWords[counter]=i;
                counter++;
            }
        }
        if(counter!=3){
            System.out.println("Invalid input. Must be 3 word with same length");
            return null;
        }
        for (int i = 0; i <3 ; i++) {
            String newWord = givenArr[indexesOfTargetWords[i]].substring(0, givenArr[indexesOfTargetWords[i]].length()-3);
            givenArr[indexesOfTargetWords[i]]=newWord.concat("$$$");
        }
        return givenArr;
    }

    public String deleteLastWord(String str){
        String trimed = str.trim();
        return trimed.substring(0,trimed.lastIndexOf(" "));
    }

    public int getNumberOfGivenWords  (String givenString){
        String temp = givenString.toLowerCase();
        char[] asCharArr = temp.toCharArray();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < asCharArr.length; i++) {
            if(!alphabet.contains(String.valueOf(asCharArr[i]))){
                asCharArr[i]=' ';
            }
        }
        String withoutPunctuation = String. valueOf(asCharArr);
        String[] onlyWords = withoutPunctuation.trim().replace("  "," ").replace("  "," ").split(" ");
        System.out.println("You've entered "+onlyWords.length+" words.");

        return onlyWords.length;
    }

    public String removeFromIndexTillLength(String str, int index, int length){
        String temp = str.substring(index, index+length);
        return str.replace(temp,"");
    }

    public String reverseString(String str){
        char[] asCharArr = str.toCharArray();
        char[] reversed = new char[asCharArr.length];
        for (int i = 0; i <asCharArr.length ; i++) {
            reversed[i] = asCharArr[asCharArr.length-i-1];
        }
        return String. valueOf(reversed);
    }


    public String onlyUniqueChars(String str){
        char [] asCharArr = str.toCharArray();
        for (int i = 0; i <asCharArr.length ; i++) {
            for (int j = i+1; j <asCharArr.length ; j++) {
                if(asCharArr[i]==asCharArr[j]){
                    asCharArr[j]='$';
                }
            }
        }
        String res=String.valueOf(asCharArr).replace("$","");
        System.out.println(res);
        return res;
    }

    public String placeSpacesAfterPunctuation(String str){

        String letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String punctuation = "[]{}:;,-_.!(\")?'";
        String indexes = "";
        for (int i = 0; i <str.length()-1 ; i++) {
            if(punctuation.contains(String.valueOf(str.charAt(i)))&(letter.contains(String.valueOf(str.charAt(i+1))))){
                indexes=indexes+" "+i;
            }
        }

        String[] indexesArr =indexes.trim().split(" ");
        int [] indexesArrInt = new int[indexesArr.length];
        int counter = 1;

        for (int i = 0; i <indexesArrInt.length ; i++) {
            indexesArrInt[i] = Integer.parseInt(indexesArr[i])+counter;
            counter++;
        }

        for (int value : indexesArrInt) {
            str = str.substring(0, value) + ' ' + str.substring(value);
        }

        return str;
    }

    public int findLengthOfShortestWord(String str){
       String temp = str.toLowerCase();
        char[] asCharArr = temp.toCharArray();
        String[] asString = new String[asCharArr.length/2];//максимально возможный размер
        String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String temp2 = "";
        int counter = 0;
        for (char c : asCharArr) {
            if (alphabet.contains(String.valueOf(c))) {
                temp2 = temp2 + c;
            } else if (!temp2.equals("")) {
                asString[counter] = temp2;
                counter++;
                temp2 = "";
            }
        }
        if(counter==0){
            return 0;
        }
        String [] wordsOnly = new String[counter];
        for (int i = 0; i <wordsOnly.length ; i++) {
            if(asString[i]!=null){
                wordsOnly[i]=asString[i];
            }else{
                break;
            }
        }
        String shortestWord = wordsOnly[0];
        for (String s : wordsOnly) {
            if (shortestWord.length() > s.length()) {
                shortestWord = s;
            }
        }

        System.out.print("Shortest word is ("+shortestWord +") and it's length is "+shortestWord.length());
        System.out.println();

        return shortestWord.length();
    }
}

