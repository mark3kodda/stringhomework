package test.java.stringHomework.tasks;

import main.java.stringHomework.tasks.SecondExercise;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class SecondExerciseTest {

    private SecondExercise cut = new SecondExercise();

    static Arguments[] intToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments(13,"13"),
                Arguments.arguments(26,"26"),
                Arguments.arguments(1,"1")

        };
    }
    @ParameterizedTest
    @MethodSource("intToStringTestArgs")
    void intToStringTest(int condition, String expected) {
        String result =cut.intToString(condition);
        Assertions.assertEquals(expected,result);
    }

    static Arguments[] doubleToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments(51.21,"51.21"),
                Arguments.arguments(14.5,"14.5"),
                Arguments.arguments(6.15,"6.15")
        };
    }
    @ParameterizedTest
    @MethodSource("doubleToStringTestArgs")
    void doubleToStringTest(double condition, String expected) {
        String result = cut.doubleToString(condition);
        Assertions.assertEquals(expected,result);
    }

    static Arguments[] sendingToIntTestArgs(){
        return new Arguments[]{
                Arguments.arguments("6",6),
                Arguments.arguments("14",14),
                Arguments.arguments("751",751)
        };
    }
    @ParameterizedTest
    @MethodSource("sendingToIntTestArgs")
    void sendingToInt(String condition,int expected) {
        int result = cut.sendingToInt(condition);
        Assertions.assertEquals(expected,result);
    }

    static Arguments[] sendingToDoubleTestArgs(){
        return new Arguments[]{
          Arguments.arguments("5.142",5.142),
          Arguments.arguments("12.72",12.72),
          Arguments.arguments("55.41",55.41)
        };
    }
    @ParameterizedTest
    @MethodSource("sendingToDoubleTestArgs")
    void sendingToDouble(String condition, double expected) {
        double result = cut.sendingToDouble(condition);
        Assertions.assertEquals(expected,result);
    }
}