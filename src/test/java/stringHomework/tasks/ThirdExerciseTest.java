package test.java.stringHomework.tasks;

import main.java.stringHomework.tasks.ThirdExercise;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class ThirdExerciseTest {

    private ThirdExercise cut  = new ThirdExercise();



    static Arguments[] replaceThreeWordsWith$TestArgs (){

        String[] condition1={"dada","mama","tata","Gandalf","Beggins"};
        String[] expected1={"d$$$","m$$$","t$$$","Gandalf","Beggins"};

        String[] condition2={"dad","mam","tat","Golf"};
        String[] expected2={"$$$","$$$","$$$","Golf"};

        String[] condition3={"dad","mm","tat","Golf"};


        return new Arguments[]{
                Arguments.arguments(condition1,4,expected1),
                Arguments.arguments(condition2,3,expected2),
                Arguments.arguments(condition3,3,null)

        };
    }
    //@Test
    @ParameterizedTest
    @MethodSource("replaceThreeWordsWith$TestArgs")
    void replaceThreeWordsWith$Test(String [] condition, int wordLenght, String[] expected) {
        String[] actual = cut.replaceThreeWordsWith$(condition,wordLenght);
        Assertions.assertArrayEquals(expected,actual);

    }

    static Arguments[] deleteLastWordTestArgs(){
        return new Arguments[]{
          Arguments.arguments("Mama mama Maria","Mama mama"),
          Arguments.arguments("You shall not pass","You shall not"),
          Arguments.arguments("And i cannot lie","And i cannot")

        };
    }

    @ParameterizedTest
    @MethodSource("deleteLastWordTestArgs")
    void deleteLastWordTest(String condition,String expected) {
        String actual = cut.deleteLastWord(condition);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] numberOfGivenWordsTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Once upon a time",4),
                Arguments.arguments("Toy story, first blood",4),
                Arguments.arguments("for the emperor",3)
        };
    }
    @ParameterizedTest
    @MethodSource("numberOfGivenWordsTestArgs")
    void getNumberOfGivenWordsTest(String condition,int expected) {
        int actual = cut.getNumberOfGivenWords(condition);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] removeFromIndexTillLengthTestArgs(){
        return new Arguments[]{
          Arguments.arguments("Chappoletto",6,4,"Chappoo"),
          Arguments.arguments("Yggdrasil",5,3,"Yggdrl"),
          Arguments.arguments("Bethesda",2,3,"Besda")
        };
    }
    @ParameterizedTest
    @MethodSource("removeFromIndexTillLengthTestArgs")
    void removeFromIndexTillLength(String condition,int index, int lengthOfCut, String expected) {
        String actual = cut.removeFromIndexTillLength(condition,index,lengthOfCut);
        Assertions.assertEquals(expected,actual);

    }

    static Arguments[] reverseStringTestArgs(){
        return new Arguments[]{
          Arguments.arguments("Nowhere","erehwoN"),
          Arguments.arguments("Copycat","tacypoC"),
          Arguments.arguments("Mama","amaM")
        };
    }

    @ParameterizedTest
    @MethodSource("reverseStringTestArgs")
    void reverseStringTest(String condition, String expected) {
        String actual = cut.reverseString(condition);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] onlyUniqueCharsTestArgs(){
        return new Arguments[]{
          Arguments.arguments("You shall not pass","You shalntp"),
          Arguments.arguments("king kong vs godzilla","king ovsdzla"),
          Arguments.arguments("naruto shippuden ultimate ninja storm","naruto shipdelmj")
        };
    }
    @ParameterizedTest
    @MethodSource("onlyUniqueCharsTestArgs")
    void onlyUniqueCharsTest(String condition,String expected) {
        String actual = cut.onlyUniqueChars(condition);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] placeSpacesAfterPunctuationTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Jack Nicholson:Here's Johnny!YouTube","Jack Nicholson: Here' s Johnny! YouTube"),
                Arguments.arguments("a!b,c.d","a! b, c. d"),
                Arguments.arguments("Terenas:What is this?What are you doing,my son?","Terenas: What is this? What are you doing, my son?"),
        };
    }
    @ParameterizedTest
    @MethodSource("placeSpacesAfterPunctuationTestArgs")
    void placeSpacesAfterPunctuationTest(String condition,String expected) {
        String actual = cut.placeSpacesAfterPunctuation(condition);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[]findLengthOfShortestWordTestArgs(){
        return new Arguments[]{
          Arguments.arguments("The shortest word is here", 2),
          Arguments.arguments("in a galaxy far far away", 1),
          Arguments.arguments("in a hole in the ground there lived a hobbit",1),
          Arguments.arguments("!  !!!!.!  !!",0),
          Arguments.arguments("",0),
          Arguments.arguments("!     !!aa!!!..,.",2)
        };
    }

    @ParameterizedTest
    @MethodSource("findLengthOfShortestWordTestArgs")
    void findLengthOfShortestWordTest(String condition,int expected) {

        int actual = cut.findLengthOfShortestWord(condition);
        Assertions.assertEquals(expected,actual);

    }
}